FROM python:3-alpine
RUN mkdir -p /opt/pythonGitlab/

ADD auth.py /opt/pythonGitlab/
ADD release.py /opt/pythonGitlab/
ADD releaseNoteParser.py /opt/pythonGitlab/
ADD release.sh /opt/pythonGitlab/
ADD requirements.txt ./
RUN ln -s /opt/pythonGitlab/release.sh /usr/bin/release \
    && chmod -R 755 /opt/pythonGitlab/ \
    && pip3 install -r requirements.txt
