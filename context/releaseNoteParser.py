#!python3
import os
import re

RELEASE_NOTES_PATH = "GLR_RELEASE_NOTES_PATH"
RELEASE_NOTES_TYPE = "GLR_RELEASE_NOTES_TYPE"

def parseReleaseNotes():
    releaseNotesType = os.environ[RELEASE_NOTES_TYPE]
    releaseNotesPath = os.environ[RELEASE_NOTES_PATH]

    supportedTypes = ["CONVENTIONAL_CHANGELOG"]
    if (releaseNotesType not in supportedTypes):
        return ""

    toReturn = "# Release Notes:  \n"

    if (releaseNotesType == "CONVENTIONAL_CHANGELOG"):
        p = re.compile('^(## [0-9]\\.[0-9]\\.[0-9]|(##|###) \\[[0-9]\\.[0-9]\\.[0-9]\\]).*\\([0-9]{4}-[0-9]{2}-[0-9]{2}\\)$')
        with open(os.path.join(releaseNotesPath)) as f:
            mode = 0
            for line in f:
                if (mode == 0):
                    if (p.search(line) != None):
                        toReturn += line
                        mode = 1

                elif (mode == 1):
                    if (p.search(line) != None):
                        mode = 2
                    else:
                        toReturn += line

                elif (mode == 2):
                    break
        f.close()

    return toReturn
