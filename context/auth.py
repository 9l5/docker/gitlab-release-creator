#!python3
import os
import gitlab

projectID = 'CI_PROJECT_ID'

def auth():
    accessTokenEnvVariable = 'GLACCESSTOKEN'
    serverURL = 'CI_SERVER_HOST'

    if ((accessTokenEnvVariable in os.environ) != True):
        raise AssertionError("GitLab Access Token wasn't set as GLACCESSTOKEN env variable!")
    if ((serverURL in os.environ) != True):
        raise AssertionError("GitLab API root url wasn't set as CI_SERVER_HOST env variable!")
    if ((projectID in os.environ) != True):
        raise AssertionError("GitLab Project ID wasn't set as CI_PROJECT_ID env variable!")

    gl = gitlab.Gitlab('https://{}'.format(os.environ[serverURL]), private_token = os.environ[accessTokenEnvVariable])
    gl.auth()
    return gl
