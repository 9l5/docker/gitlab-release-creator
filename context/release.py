#!python3
import auth
import gitlab
import os
import sys
import releaseNoteParser

# check if we have a tag set
tagName = 'CI_COMMIT_TAG'
if ((tagName in os.environ) != True):
    raise AssertionError("GitLab Tag wasn't set as CI_COMMIT_TAG env variable!")

# authenticate with gitlab and get our project
gl = auth.auth()
project = gl.projects.get(os.environ[auth.projectID])

# get paths for all files
files = list()
for file in sys.argv[1:]:
    files.append(os.path.join(os.getcwd(), file))

uploadedFiles = list()

print('start uploading files for release')
for file in files:
    print('uploading file: {}'.format(file))
    uploadedFiles.append(project.upload(os.path.basename(file), filepath=file))
print('all files uploaded\n')

releaseDescription = ""
if (len(files) > 0):
    releaseDescription += "# Downloads:  \n"
    for file in uploadedFiles:
        releaseDescription += file['markdown'] + '  \n'

if (releaseNoteParser.RELEASE_NOTES_TYPE in os.environ and releaseNoteParser.RELEASE_NOTES_PATH in os.environ):
    releaseDescription += releaseNoteParser.parseReleaseNotes()

print('creating new release for tag {}'.format(os.environ[tagName]))
release = project.releases.create(
    {
        'name':os.environ[tagName],
        'tag_name':os.environ[tagName],
        'description':releaseDescription
    })
